---
title: CS373 Spring 2020: Vy Phan
author: 
authorURL: 
authorFBID: 
---
<h1>Blog 5</h1>

**What did you do this past week?**

I went through several more interview this week. There are more rounds to continue with. A majority of this week was devoted to finishing up the IDB project for software engineering.
It is no secret on our commits that we started and worked on our project late so I will admit it here. Nonetheless, we managed to finish it in time with all the requirements done. There was a lot of learning involved as well as troubleshooting and bug-fixing. Stressful yet fun. I am glad I have the group I have now.

**What's in your way?**

My dreaded statistics homework is coming up again. I have yet to take a look at it but will soon on Sunday. Though my software engineering group has agreed on not procrastinating on the project, I fear this next phase is still very troublesome for my skilled and knowledgeable group. If anyone knows how to automate https with AWS Elastic Beanstalk, please let me know.

**What will you do next week?**

I will continue to learn React on my own time. I do have more interviews and will continue applying to jobs. My software engineering group definitely needs to meet to decide our next steps in implementing phase two of the IDB project. I fear I may not be able to fill multiple roles anymore and may have to specialize. I hope not, because working on the full stack application is very fun.

**What was your experience of comprehensions, generators, and yield?**

List comprehensions was very much needed to be taught to me. I am glad it was explained step by step on how it works. Generators are quite an interesting function but I cannot imagine creating one but I can see that some libraries may rely on it. Ultimately, I think yield was the most interesting topic of the week. Something about the way a function containing yield will run the rest of its code when next() is called on it until it encounters a yield seems precise and potent. 

**What made you happy this week?**

We finished the first IDB project right before the deadline. So though we cut it close and made it, it would be less stressful if we were not to face that again. I am also happy to see that I can apply some of the things I have learned from online tutorials or referring to past knowledge into our project.

**What's your pick-of-the-week or tip-of-the-week?**

At least in the academic settings among peers, I think it is valuable to ask each of our groupmates what are they doing. This is not to gauge their productivity, but instead to learn parts of the application that you do not understand. Maybe you do not want to be all over the place with knowledge of the full stack, but I believe it is necessary to understand the high-level details of what your fellow group members are doing to make your website work.

<img src="https://gitlab.com/vphan6896/docusaurus/raw/master/website/static/img/headshot.jpg" width="50%" height="auto" alt="Headshot Image">
